package com.company;

import javax.persistence.*;

@Entity
@Table(name = "test")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "column_1")
    private String name;
    @Column(name = "column_2")
    private int ID;

    public Product(){}
    public Product(String name, int ID) {
        this.name = name;
        this.ID = ID;
    }
    public void setID(int ID) {
        this.ID = ID;
    }
    public int getID() {
        return ID;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
}
