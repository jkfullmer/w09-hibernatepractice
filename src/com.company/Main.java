/*
Title: W09 HibernatePractice
Purpose: To show that I can retrieve data from a table using hibernate
Author: Joshua Fullmer
Date: 11/10/2021
Notes: I followed the course videos and examples in order to get to this point.
 */

package com.company;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("This program shows which products we have in stock.");
        DAO dao = DAO.getInstance();
        List<Product> p = dao.getProducts();
        for (Product i : p) {
            System.out.println(i);
        }
        System.out.println("That is all. Goodbye!");
    }
}
