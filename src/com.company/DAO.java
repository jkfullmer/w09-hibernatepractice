package com.company;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class DAO {
    SessionFactory factory = null;
    Session session = null;
    private static DAO single_instance = null;
    private DAO() {
        factory = HibernateUtils.getSessionFactory();
    }

    public static DAO getInstance() { // This is what makes it a singleton
        if (single_instance == null) {
            single_instance = new DAO();
        }
        return single_instance;
    }

    public List<Product> getProducts() {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.company.Product";
            List<Product> product = (List<Product>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return product;
        } catch (Exception e) {
            System.out.println(e);
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
}
